
public final class Fraction {

	private final int num;
	private final int den;

	public static final Fraction ZERO = new Fraction();
	public static final Fraction UN = new Fraction(1,1);
	
	public Fraction(int num, int den){
		this.num = num;
		this.den = den;
	}
	
	public Fraction(){
		this.num = 0;
		this.den = 1;
	}

	public Fraction(int num){
	    this.num = num;
	    this.den = 1;
    }
	
    public int getNum(){
	    return num;
    }

    public int getDen(){
        return den;
    }

    public double getFloat() {
        double res = 0;
        if (this.den != 0) {
            res = this.num / this.den;
        }
        return res;
    }
    
    public String getString() {
    	return "Numerateur = " + num + " Denominateur = " + den;
    }
}
